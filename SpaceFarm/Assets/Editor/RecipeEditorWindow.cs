﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class RecipeEditorWindow : EditorWindow
{

    public RecipeDataList recipeData;

    private string gameDataProjectFilePath = "/JSONdata/recipeData.json";

    [MenuItem("Window/Recipe Editor")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(RecipeEditorWindow)).Show();
    }

    void OnGUI()
    {
        if (recipeData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("recipeData");
            EditorGUILayout.PropertyField(serializedProperty, true);

            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save data"))
            {
                SaveGameData();
            }
        }

        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }
    }

    private void LoadGameData()
    {
        string filePath = Application.dataPath + gameDataProjectFilePath;

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            //recipeData = JsonUtility.FromJson<RecipeData>(dataAsJson);
            recipeData = JsonUtility.FromJson<RecipeDataList>(dataAsJson);
        }
        else
        {
            recipeData = new RecipeDataList();
            //recipeData.Add(new RecipeData());
        }
    }

    private void SaveGameData()
    {

        string dataAsJson = JsonUtility.ToJson(recipeData);

        string filePath = Application.dataPath + gameDataProjectFilePath;
        File.WriteAllText(filePath, dataAsJson);

    }
}