﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  InventoryManager class handles the inventory grid and will store the references to IntentoryItems.  It should
 *  act as an in-between that only handles inventory-based functions, passing all item usage commands on to the
 *  InventoryItem itself
*/

public class InventoryManager : MonoBehaviour
{
    InventoryItem[] inv;
    int maxSlots = 15;
    int maxHotbarSlots = 10;        //todo: this currently exists in GUIItemHotbarPanel also when it shouldn't be set in 2 places
    int selectedIndex;

    private bool isAutoSort;
    private bool isPlayerInventory;

    private GUIItemSlotPanel itemSlotPanel;
    private GUIItemHotbarPanel itemHotbarPanel;
    private bool hasGUI = true;  // default to true

    private InventoryItem[] hotbarInv;
    private InventoryItem[] mainInv;

    // Drag and drop function variables
    InventoryItem tempDrag1, tempDrag2;

    // Use this for initialization
    void Start ()
    {
        selectedIndex = 0;

        itemSlotPanel = GameObject.FindWithTag("ItemSlotPanel").GetComponent<GUIItemSlotPanel>();

        if (itemSlotPanel == null)
        {
            Debug.Log("Didn't find ItemSlotPanel!");
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Create the inventory array with the given size
    public void Initialize(int invSlots, bool autoSort)
    {
        isAutoSort = autoSort;
        maxSlots = invSlots;
        inv = new InventoryItem[maxSlots];

    }

    // Player-specific creation
    public void InitializeForPlayer()
    {
        inv = new InventoryItem[maxSlots];
        isAutoSort = false;
        isPlayerInventory = true;

        itemHotbarPanel = GameObject.FindWithTag("HotbarPanel").GetComponent<GUIItemHotbarPanel>();
        if (itemHotbarPanel == null)
        {
            Debug.Log("Player inventory couldn't find hotbar!");
        }
        else
        {
            UpdateHotbarIndex(0);
        }

        //Debug -- remove these when no longer needed
        InstantiateAndAddItem(Resources.Load("buildingStorageChest0") as GameObject, 2);
        AddItem(Instantiate((Resources.Load("cropTurnip0") as GameObject).GetComponent<InventoryItem>(), transform));
        //AddItem(Instantiate((Resources.Load("buildingMachineSeedmaker0") as GameObject).GetComponent<InventoryItem>(), transform));
        AddItem(Instantiate((Resources.Load("toolMattock0") as GameObject).GetComponent<InventoryItem>(), transform));
        AddItem(Instantiate((Resources.Load("toolWatering0") as GameObject).GetComponent<InventoryItem>(), transform));
        AddItem(Instantiate((Resources.Load("toolSeedbagTurnip0") as GameObject).GetComponent<InventoryItem>(), transform));
        AddItem(Instantiate((Resources.Load("toolSeedbagTree0") as GameObject).GetComponent<InventoryItem>(), transform));
        //AddItem(Instantiate((Resources.Load("buildingNetworkCable0") as GameObject).GetComponent<InventoryItem>(), transform));
        //AddItem(Instantiate((Resources.Load("buildingMachineSawmill0") as GameObject).GetComponent<InventoryItem>(), transform));
        AddItem(Instantiate((Resources.Load("resourceWoodLog0") as GameObject).GetComponent<InventoryItem>(), transform));
        AddItem(Instantiate((Resources.Load("buildingPlotMarker0") as GameObject).GetComponent<InventoryItem>(), transform));

        Debug.Log("Init player inventory");
    }

    // Scroll through hotbar
    public void ChangeSelected(float x)
    {
        /*
        if (x > 0)
        {
            
            if ( (selectedIndex + 1) > (maxHotbarSlots - 1))
            {
                selectedIndex = 0;
            }
            else
            {
                selectedIndex += 1;
            }
            

            selectedIndex = (selectedIndex + 1) > (maxHotbarSlots - 1) ? 0 : selectedIndex + 1;
        }
        else
        {
            
            if ( (selectedIndex - 1) < 0)
            {
                selectedIndex = (maxHotbarSlots - 1);
            }
            else
            {
                selectedIndex -= 1;
            }
            

            selectedIndex = (selectedIndex - 1) < 0 ? (maxHotbarSlots - 1) : (selectedIndex - 1);
        }
        */

        //Prevent index from incrementing or decrementing out of range for hotbar array
        selectedIndex += (int)Math.Round(x, MidpointRounding.AwayFromZero);
        selectedIndex =
            selectedIndex == maxHotbarSlots ?
            0
            : selectedIndex == -1 ? maxHotbarSlots - 1 : selectedIndex;


        UpdateHotbarIndex(selectedIndex);

        // TODO: remove after testing
        if (inv[selectedIndex] == null)
        {
            Debug.Log("Active index: " + selectedIndex + " using NULL");
        }
        else
        {
            Debug.Log("Active index: " + selectedIndex + " using item " + inv[selectedIndex].itemName
                + " with count " + inv[selectedIndex].itemCount);
        }
    }

    // Set index to a specific value, returns false if invalid index
    public bool ChangeSelectedToIndex(int x)
    {
        if (x >= 0 && x < maxHotbarSlots)
        {
            selectedIndex = x;
            return true;
        }
        else
        {
            return false;
        }
    }

    // Use currently selected item
    public void UseSelected(WorldBlock hitObj, Vector3 hitPosition)
    {
        if (inv[selectedIndex] != null)
        {
            InventoryItem tempItem = inv[selectedIndex];
            bool wasUsed = tempItem.UseItem(hitObj, hitPosition, tempItem);

            if (wasUsed && tempItem.isSingleUse)
            {
                tempItem.itemCount -= 1;
                if (tempItem.itemCount <= 0)
                {
                    DropItem(selectedIndex, false);
                }
                else
                {
                    UpdateInv(selectedIndex, tempItem);
                }
            }
        }
        else
        {
            Debug.Log("Use failed: no item selected");
        }
    }

    // Check if the inventory is empty
    //TODO: prob want to test lambda shit more thoroughly than you did
    public bool IsEmpty()
    {
        return (Array.FindIndex(inv, invItem => invItem != null) == -1) ? true : false;
    }

    // Activate while holding currently selected item
    public void ActivateSelected(WorldBlock hitObj)
    {
        if (hitObj != null)
        {
            hitObj.ActivateFunction();
        }
        else
        {
            Debug.Log("Do I need to do something with this item if there's no worldblock here?");
        }
    }

    // Check for the first available empty slot; if -1 then no empty index found
    int GetEmptySlot()
    {
        return Array.FindIndex(inv, invItem => invItem == null);
    }

    // Look for an item with matching type.  If it has room in its stack, return its index
    //TODO: think if they pass max stack size then they don't combine-- need to test this
    int FindMatchingForAdd(InventoryItem item)
    {
        //Find index of inv where there exists an InventoryItem that matches input itemID and can hold the input itemCount 
        //InventoryItem matchItem = item.GetComponent<InventoryItem>();
        return Array.FindIndex(inv, invItem =>
        {
            InventoryItem invDataItem = invItem == null ? null : invItem;
            return invDataItem != null &&
            invDataItem.itemID == item.itemID &&
            invDataItem.itemCount + item.itemCount < invDataItem.stackSize;
        });

    }

    // Try to add new item to inventory.  If no slot available, return false
    public bool AddItem(InventoryItem item)
    {
        //Debug.Log("Attempting to add item: " + item.name);
        int addSlot = -1;
        addSlot = FindMatchingForAdd(item);

        if (addSlot == -1)
        {
            addSlot = GetEmptySlot();

            if (addSlot > -1)
            {
                //inv[addSlot] = item;
                UpdateInv(addSlot, item);
                return true;
            } 
        }
        else
        {
            InventoryItem tempItem = inv[addSlot];
            tempItem.itemCount += item.itemCount;
            UpdateInv(addSlot, tempItem);
            return true;
        }

        Debug.Log("Didn't add item: " + item.name);
        return false;
    }

    //Instantiate the new object and attempt to add it to inventory; destroy if unable to add
    //Parameters after GameObject can be used to change settings, if 0 then it uses defaults
    public bool InstantiateAndAddItem(GameObject newObj, int itemCount)
    {
        GameObject itemObj = Instantiate(newObj as GameObject, transform);
        InventoryItem item = itemObj.GetComponent<InventoryItem>();
        InventoryItem tempItem = null;

        if (itemCount != 0)
        {
            item.itemCount = itemCount; 
        }

        bool canAdd = false;

        int addSlot = -1;
        addSlot = FindMatchingForAdd(item);

        if (addSlot == -1)
        {
            addSlot = GetEmptySlot();

            if (addSlot > -1)
            {
                //inv[addSlot] = item;
                //UpdateInv(addSlot, item);
                canAdd = true;
                tempItem = item;
            }
        }
        else
        {
            tempItem = inv[addSlot];
            tempItem.itemCount += item.itemCount;
            //UpdateInv(addSlot, tempItem);
            canAdd = true;
        }

        if (canAdd)
        {
            UpdateInv(addSlot, tempItem);
        }

        return false;
    }

    // Try to drop item from specified index.  If drop unsuccessful, return false
    public bool DropItem(int dropIndex, bool dropIntoWorld)
    {
        if (inv[dropIndex] == null)
        {
            return false;
        }
        else
        {
            UpdateInv(dropIndex, null);
            return true;
        }
    }

    // Subtract the given number from the count of an item stack
    //TODO: haven't actually tested this yet
    public bool SubtractItemByCount(int subIndex, int subCount)
    {
        InventoryItem tempItem = inv[subIndex];
        if (tempItem.itemCount < subCount)
        {
            Debug.Log("Tried to remove more of this item than you have!");
            return false;
        }
        else
        {
            tempItem.itemCount -= subCount;
            Debug.Log("Subtracted items, left with this many: " + tempItem.itemCount);
            if (tempItem.itemCount <= 0)
            {
                bool didDrop = DropItem(subIndex, false);
                if (!didDrop)
                {
                    Debug.Log("Something went terribly wrong in SubtractItemByCount");
                    return false;
                }
            }
            else
            {
                UpdateInv(subIndex, tempItem);
            }
            return true;
        }
    }

    // Take item from 0 index and send it to another entity's inventory or into the world
    // TODO: add code for dropping into world
    public InventoryItem PopItem(bool dropIntoWorld)
    {
        if (!IsEmpty())
        {
            InventoryItem poppedItem = inv[0];
            bool wasDropped = DropItem(0, false);
            if (wasDropped)
            {
                return poppedItem;
            }
            else
            {
                Debug.Log("PopItem failed to drop said item from inventory and will return null");
            }
        }
        return null;
    }

    // If holding item, try to place it in world block inventory
    // If not holding item, try to take from world block inventory
    public void QuickActivate(WorldBlock hitObj)
    {
        bool wasPlaced = false;
        bool wasDropped = false;

        InventoryItem takenItem = null;
        bool wasAdded = false;

        if (inv[selectedIndex] != null)
        {
            wasPlaced = hitObj.TakeItem(inv[selectedIndex]); 
        }
        else
        {
            takenItem = hitObj.GiveItem();
        }

        //if wasPlaced is good, then remove that item from player's inventory
        if (wasPlaced)
        {
            wasDropped = DropItem(selectedIndex, false);

            if (!wasDropped)
            {
                Debug.Log("Placed item, but failed to drop from old inventory somehow");
                // TODO: take it away from world block inventory because duplicate?
            }
        }

        // if takenItem ended up with a value, add it to player's inventory
        if (takenItem != null)
        {
            int emptySlot = GetEmptySlot();
            wasAdded = AddItem(takenItem);

            if (!wasAdded)
            {
                Debug.Log("Item taken from world block but not added to player inventory");
                // TODO: add it back to world block inventory because item loss?
            }
        }
    }

    // Shuffle contents so everything starts at index 0 and there are no gaps between items
    // Recommend only calling it thru UpdateInv function
    private void SortItems()
    {
        // Just a bubble sort
        for (int i = 0; i < inv.Length; ++i)
        {
            for (int j = 0; j < inv.Length - i - 1; ++j)
            {
                if (inv[j] == null && inv[j+1] != null)
                {
                    InventoryItem tempItem = inv[j+1];
                    inv[j] = tempItem;
                    inv[j + 1] = null;
                }
            }
        }

            DebugListItems();
    }

    // Use to list all the items in an inventory
    public void DebugListItems()
    {
        Debug.Log("Showing dump of inventory");
        for (int i = 0; i < inv.Length; ++i)
        {
            string tempID = "";
            if (inv[i] != null)
            {
                tempID = inv[i].itemName;
            }
            else
            {
                tempID = "";
            }

            Debug.Log("Index: " + i + " has item: " + tempID);
        }
        Debug.Log("Dump complete");
    }

    // Fetch item IDs for everything currently in inventory
    public List<InventoryItem> FetchItemList()
    {
        List<InventoryItem> itemIDs = new List<InventoryItem>();

        for (int i = 0; i < inv.Length; ++i)
        {
            if (inv[i] != null)
            {
                itemIDs.Add(inv[i].GetComponent<InventoryItem>()); 
            }
        }

        return itemIDs;
    }

    // Fetch GameObjects currently in inventory
    public InventoryItem[] FetchGameObjectList()
    {
        return inv;
    }

    public void DebugDump(WorldBlock hitObj)
    {
        hitObj.DebugDump();
    }

    public void OpenGUI()
    {
        //Should take the itemSlotPanel and update it to show item icons and counts
        //ActivatePanel takes an array
        Debug.Log("Player trying to open inventory, yay");
        
        if (isPlayerInventory)
        {
            //TODO: check that inv.length is actually bigger than maxHotbarSlots

            hotbarInv = new InventoryItem[maxHotbarSlots];
            mainInv = new InventoryItem[inv.Length];

            SeparateHotbarInv();

            itemSlotPanel.ActivatePanel(mainInv);

            itemHotbarPanel.LoadItems(hotbarInv);
            itemHotbarPanel.SetVisible(true);
        }
        else
        
        
        {
            itemSlotPanel.ActivatePanel(inv);
        }

    }

    // For the internal inventories that don't need GUI functions
    public void SetToNoGUI()
    {
        hasGUI = false;
    }

    //Separate inventory into a hotbarInv array and a mainInv array
    public void SeparateHotbarInv()
    {
        hotbarInv = new InventoryItem[maxHotbarSlots];
        mainInv = new InventoryItem[inv.Length];

        int j = 0, k = 0;

        for (int i = 0; i < inv.Length; ++i)
        {

            if (i < hotbarInv.Length)
            {
                hotbarInv[j] = inv[i];
                ++j;
            }
            else
            {
                mainInv[k] = inv[i];
                ++k;
            }
            
        }
    }

    //Update hotbar to show which index is currently active
    public void UpdateHotbarIndex(int index)
    {
        itemHotbarPanel.HighlightSlot(index, maxHotbarSlots - 1);
    }

    //A change has been made to the inventory, so sort if permitted and then update the GUI if GUI present
    //Takes index and item to replace existing item with
    private void UpdateInv(int index, InventoryItem newItem)
    {
        inv[index] = newItem;

        //Debug.Log("Updating inventory");
        if (isAutoSort)
        {
            SortItems();
        }

        if (isPlayerInventory)
        {
            SeparateHotbarInv();
            itemHotbarPanel.LoadItems(hotbarInv);
            if (itemSlotPanel.parentIsVisible)
            {
                itemSlotPanel.LoadItems(mainInv); 
            }
        }
        else
        {
            if (hasGUI)
            {
                if (itemSlotPanel.parentIsVisible)
                {
                    itemSlotPanel.LoadItems(inv);
                } 
            }
        }
        
    }
}
