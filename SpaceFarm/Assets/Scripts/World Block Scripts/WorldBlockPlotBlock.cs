﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBlockPlotBlock : WorldBlockBuilding
{
    [Header("Farming plot settings")]
    public WorldBlockPlot plotPrefab;
    public int maxDetectDistance = 8;

    //TODO: made public for testing, prob should hide
    public WorldBlockPlot ownerPlot;

    //TODO: made public for testing, prob should hide
    public bool checkNorth;
    public bool checkSouth;
    public bool checkEast;
    public bool checkWest;

    public enum AddToPlotResults
    {
        ADDED,
        MATCHED,
        FAILED
    }

    // Use this for initialization
    void Start ()
    {
        //Debug.Log("Actual start called");
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void Awake()
    {
        //Debug.Log("Awake called");
    }

    // Start is shit so initializing stuff here
    public override void GetPlaced()
    {
        checkNorth = true;
        checkSouth = true;
        checkEast = true;
        checkWest = true;

        ownerPlot = null;

        CheckForMarkers();
    }

    // Calls checkalongaxis for all 4 directions
    private void CheckForMarkers()
    {
        WorldBlockPlotBlock foundMarker = null;

        
        // +x direction
        if (checkEast)
        {
            foundMarker = CheckAlongAxis(new Vector3(1, 0, 0), maxDetectDistance, true);
            if (foundMarker != null)
            {
                checkEast = false;
                checkWest = false;
                if (foundMarker != this)
                {
                    foundMarker.checkWest = false;
                    foundMarker.checkEast = false; 
                }
            } 
        }

        // -x direction
        if (checkWest)
        {
            foundMarker = CheckAlongAxis(new Vector3(-1, 0, 0), maxDetectDistance, true);
            if (foundMarker != null)
            {
                checkEast = false;
                checkWest = false;
                if (foundMarker != this)
                {
                    foundMarker.checkWest = false;
                    foundMarker.checkEast = false; 
                }
            } 
        }

        // +z direction
        if (checkNorth)
        {
            foundMarker = CheckAlongAxis(new Vector3(0, 0, 1), maxDetectDistance, true);
            if (foundMarker != null)
            {
                checkNorth = false;
                checkSouth = false;
                if (foundMarker != this)
                {
                    foundMarker.checkNorth = false;
                    foundMarker.checkSouth = false; 
                }
            } 
        }

        // -z direction
        if (checkSouth)
        {
            foundMarker = CheckAlongAxis(new Vector3(0, 0, -1), maxDetectDistance, true);
            if (foundMarker != null)
            {
                checkNorth = false;
                checkSouth = false;
                if (foundMarker != this)
                {
                    foundMarker.checkNorth = false;
                    foundMarker.checkSouth = false; 
                }
            } 
        }

    }

    // If a marker was found, it will be returned
    // Can return "this" if a marker was found but no changes were made to it
    // Will return null if marker not found or found a marker whose plot it couldn't/ didn't join
    // If tryToAdd is true, it'll attempt to add the "this" marker to the plot of the one it found
    public WorldBlockPlotBlock CheckAlongAxis(Vector3 inAxis, float checkDistance, bool tryToAdd)
    {
        RaycastHit hitObj;
        WorldBlockPlotBlock hitBlock, returnBlock;
        returnBlock = null;

        float verticalOffset = 0.5f;

        Vector3 startPos = new Vector3(transform.position.x, verticalOffset, transform.position.z);

        if (Physics.Raycast(startPos, inAxis, out hitObj, checkDistance))
        {
            // check if hit object is another marker
            if (hitObj.collider.gameObject.GetComponent<WorldBlockPlotBlock>())
            {
                // plot marker found
                hitBlock = hitObj.collider.gameObject.GetComponent<WorldBlockPlotBlock>();

                if (MarkerCanAcceptDirection(hitBlock, inAxis)  && tryToAdd)
                {
                    AddToPlotResults addResults = AddToPlot(this, hitBlock);
                    switch (addResults)
                    {
                        case AddToPlotResults.ADDED:
                            {
                                returnBlock = hitBlock;
                                break;
                            }
                        case AddToPlotResults.MATCHED:
                            {
                                returnBlock = this;
                                break;
                            }
                        case AddToPlotResults.FAILED:
                            {
                                returnBlock = null;
                                break;
                            }
                        default:
                            {
                                Debug.Log("PlotBlock method returned invalid results!");
                                break;
                            }
                    } 
                }

                if (!tryToAdd)
                {
                    returnBlock = hitBlock;
                }

            }
            // Hit something but it wasn't a marker
            else
            {
                returnBlock = null;
            }
            
        }

        return returnBlock;

    }

    /*
     * Tries to add the markers to a plot
     * If no plot exists, create new one & return "added"
     * If existing marker has a plot and new one doesn't, use existing and return "added"
     * If new marker wasn't successfully added to a plot, return "failed"
     * If new marker and old marker have same plot, return "matched"
     */
    public AddToPlotResults AddToPlot(WorldBlockPlotBlock firstMarker, WorldBlockPlotBlock secondMarker)
    {
        WorldBlockPlot tempPlot, oldPlot;
        bool createdNewPlot = false;

        // If our new marker found a plot and it doesn't match the second marker's plot, decide which to keep
        if (firstMarker.ownerPlot != null && firstMarker.ownerPlot != secondMarker.ownerPlot)
        {

            if (firstMarker.ownerPlot.CountPresentMarkers() < secondMarker.ownerPlot.CountPresentMarkers())
            {
                if (!secondMarker.ownerPlot.AddMarker(firstMarker))
                {
                    // FirstMarker was rejected from the plot
                    return AddToPlotResults.FAILED;
                }
                else
                {
                    oldPlot = firstMarker.ownerPlot;
                    oldPlot.RemoveMarker(firstMarker);
                    firstMarker.ownerPlot = secondMarker.ownerPlot;
                    return AddToPlotResults.ADDED;
                }

            }

            // Keeping firstMarker in its existing plot
            return AddToPlotResults.FAILED;
        }

        // Don't bother adding because it's already in the plot
        if ((firstMarker.ownerPlot != null || secondMarker.ownerPlot != null) && firstMarker.ownerPlot == secondMarker.ownerPlot)
        {
            return AddToPlotResults.MATCHED;
        }

        // If marker that already exists has a plot, use it
        tempPlot = secondMarker.ownerPlot;

        // If neither marker already had a parent plot, make one
        if (tempPlot == null)
        {
            tempPlot = Instantiate(plotPrefab, transform);
            createdNewPlot = true;
        }

        if (!tempPlot.AddMarker(firstMarker))
        {
            // FirstMarker was rejected from the plot
            return AddToPlotResults.FAILED;
        }
        else
        {
            firstMarker.ownerPlot = tempPlot;
        }

        if (createdNewPlot)
        {
            secondMarker.ownerPlot = tempPlot;
            if (!tempPlot.AddMarker(secondMarker))
            {
                // If this happens then something has gone terribly wrong
                return AddToPlotResults.FAILED;
            }
            tempPlot.transform.SetParent(null);
        }

        return AddToPlotResults.ADDED;
    }

    public void GetRemovedFromPlot()
    {
        transform.SetParent(null);
        ownerPlot = null;

        ResetDirectionChecks();

    }

    public void ResetDirectionChecks()
    {
        checkNorth = true;
        checkSouth = true;
        checkEast = true;
        checkWest = true;

        CheckForMarkers();
    }

    // See if the given marker can check for connections coming from the given direction
    private bool MarkerCanAcceptDirection(WorldBlockPlotBlock hitBlock, Vector3 inAxis)
    {
        string directionFrom = "";

        // +x
        if (inAxis.x == 1 && inAxis.y == 0 && inAxis.z == 0)
        {
            directionFrom = "west";
        }

        // -x
        if (inAxis.x == -1 && inAxis.y == 0 && inAxis.z == 0)
        {
            directionFrom = "east";
        }

        // +z
        if (inAxis.x == 0 && inAxis.y == 0 && inAxis.z == 1)
        {
            directionFrom = "south";
        }

        // -z
        if (inAxis.x == 0 && inAxis.y == 0 && inAxis.z == -1)
        {
            directionFrom = "north";
        }

        switch (directionFrom)
        {
            case "east":
                {
                    return hitBlock.checkEast;
                }
            case "west":
                {
                    return hitBlock.checkWest;
                }
            case "north":
                {
                    return hitBlock.checkNorth;
                }
            case "south":
                {
                    return hitBlock.checkSouth;
                }
        }

        return false;
    }
}
