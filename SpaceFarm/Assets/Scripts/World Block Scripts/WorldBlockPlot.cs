﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBlockPlot : WorldBlock
{
    //TODO: just made these public for testing, prob should hide
    public WorldBlockPlotBlock[] plotMarkers;
    public List<WorldBlockPlotBlock> openCorners;
    public bool allMarkersPresent;

    private PlotUnit[,] plotUnits;

    public InventoryManager inventoryPrefab;
    private InventoryManager inv;
    private int maxSlots = 4;
    private bool isAutoSort = false;

    private float tillingDelay = 1.0f;
    private float wateringDelay = 1.0f;
    private bool didTillGround = false;
    private bool didTillDirt = false;
    private bool didWaterDirt = false;

	// Use this for initialization
	void Start ()
    {
        Debug.Log("Called start on plot");
	}

    private void Awake()
    {
        Debug.Log("Called awake on plot");
        plotMarkers = new WorldBlockPlotBlock[4];
        openCorners = new List<WorldBlockPlotBlock>();
        allMarkersPresent = false;

        Debug.Log("Trying to setup plot inventory");
        inv = Instantiate(inventoryPrefab, transform);
        inv.Initialize(maxSlots, isAutoSort);
        inv.SetToNoGUI();
        //inv.AddItem(Instantiate((Resources.Load("toolMattock0") as GameObject).GetComponent<InventoryItem>(), transform));
        inv.InstantiateAndAddItem(Resources.Load("toolMattock0") as GameObject, 1);
    }

    // Update is called once per frame
    void Update ()
    {
        if (!didTillDirt && didTillGround)
        {
            didTillDirt = true;
            StartCoroutine("TillUnits");

        }

    }

    // Attempt to add a marker and return false if there wasn't room for it
    public bool AddMarker(WorldBlockPlotBlock inMarker)
    {
        int openMarker = FindOpenMarkerSlot();
        if (openMarker >= 0)
        {
            // If this is the 4th marker, check it it would complete the plot
            if (openMarker == 3)
            {
                openCorners.Clear();

                // See which corners are open and check if new marker would match them both
                for (int i = 0; i < plotMarkers.Length; ++i)
                {
                    if (plotMarkers[i] != null)
                    {
                        if (plotMarkers[i].checkEast || plotMarkers[i].checkWest || plotMarkers[i].checkNorth || plotMarkers[i].checkSouth)
                        {
                            openCorners.Add(plotMarkers[i]);
                        } 
                    }
                }

                if (openCorners.Count == 2)
                {
                    WorldBlockPlotBlock eastWestMarker, northSouthMarker, foundMarker;
                    bool eastWestMatch = false;
                    bool northSouthMatch = false;

                    if (openCorners[0].checkEast || openCorners[0].checkWest)
                    {
                        eastWestMarker = openCorners[0];
                        northSouthMarker = openCorners[1];
                    }
                    else
                    {
                        eastWestMarker = openCorners[1];
                        northSouthMarker = openCorners[0];
                    }

                    //Got the two markers, now check along axes to see if they match
                    foundMarker = inMarker.CheckAlongAxis(new Vector3(1, 0, 0), inMarker.maxDetectDistance, false);
                    if (foundMarker != null && foundMarker.GetInstanceID() == eastWestMarker.GetInstanceID())
                    {
                        eastWestMatch = true;
                    }
                    foundMarker = inMarker.CheckAlongAxis(new Vector3(-1, 0, 0), inMarker.maxDetectDistance, false);
                    if (foundMarker != null && foundMarker.GetInstanceID() == eastWestMarker.GetInstanceID())
                    {
                        eastWestMatch = true;
                    }

                    foundMarker = inMarker.CheckAlongAxis(new Vector3(0, 0, 1), inMarker.maxDetectDistance, false);
                    if (foundMarker != null && foundMarker.GetInstanceID() == northSouthMarker.GetInstanceID())
                    {
                        northSouthMatch = true;
                    }
                    foundMarker = inMarker.CheckAlongAxis(new Vector3(0, 0, -1), inMarker.maxDetectDistance, false);
                    if (foundMarker != null && foundMarker.GetInstanceID() == northSouthMarker.GetInstanceID())
                    {
                        northSouthMatch = true;
                    }

                    if (northSouthMatch && eastWestMatch)
                    {
                        plotMarkers[openMarker] = inMarker;

                        Debug.Log("Added marker to plot " + GetInstanceID());
                        inMarker.transform.SetParent(transform);
                        RefreshParenting();
                    }
                    else
                    {
                        Debug.Log("Existing markers think this new one wasn't in grid, not adding it");
                        return false;
                    }

                }
                
            }
            // It wasn't the 4th marker, so just add it
            else
            {
                plotMarkers[openMarker] = inMarker;
                Debug.Log("Added marker to plot " + GetInstanceID());
                inMarker.transform.SetParent(transform);
                RefreshParenting();
            }

            if (FindOpenMarkerSlot() < 0)
            {
                ActivatePlot();
                //allMarkersPresent = true;
                //SortMarkersIntoCorners();
            }

            return true; 
            
        }
        else
        {
            Debug.Log("Don't have room for marker in this plot " + GetInstanceID());
        }
        return false;
    }

    // Plot is complete, so get everything started up
    private void ActivatePlot()
    {
        Debug.Log("Activating plot");
        allMarkersPresent = true;
        SortMarkersIntoCorners();
        GetInteriorUnits();
        StartCoroutine("TillUnits");
        //didTillGround = true;
    }

    // Trying to fix a stupid Start bug that causes markers to get unparented in the hierarchy
    private void RefreshParenting()
    {
        for (int i = 0; i < plotMarkers.Length; ++i)
        {
            if (plotMarkers[i] != null)
            {
                plotMarkers[i].transform.SetParent(transform);
            }
        }
    }

    // Get open index in plotMarkers array; if none, return -1
    public int FindOpenMarkerSlot()
    {
        int slot = -1;

        for (int x = 0; x < plotMarkers.Length; ++x)
        {
            if (plotMarkers[x] == null)
            {
                slot = x;
                break;
            }
        }

        return slot;
    }

    public void RemoveMarker(WorldBlockPlotBlock oldMarker)
    {
        for (int i = 0; i < plotMarkers.Length; ++i)
        {
            if (plotMarkers[i].GetInstanceID() == oldMarker.GetInstanceID())
            {
                plotMarkers[i] = null;
                Debug.Log("Removed marker from plot " + GetInstanceID());
                oldMarker.GetRemovedFromPlot();
                break;
            }
        }

        // Since you removed a marker, you'll need to reset the direction checks on the remaining markers
        // TODO: need to make sure this doesn't have undesirable results
        for (int i = 0; i < plotMarkers.Length; ++i)
        {
            if (plotMarkers[i] != null)
            {
                plotMarkers[i].ResetDirectionChecks();
            }
        }
    }

    public bool GetAllMarkersPresent()
    {
        return allMarkersPresent;
    }

    public int CountPresentMarkers()
    {
        int count = 0;
        for (int i = 0; i < plotMarkers.Length; ++i)
        {
            if (plotMarkers[i] != null)
            {
                ++count;
            }
        }

        return count;
    }

    /*
     * Sort the markers accordingly:
     * [0] = SW
     * [1] = NW
     * [2] = SE
     * [3] = NE
     */

    private void SortMarkersIntoCorners()
    {
        
        //Debug.Log("Sorting markers, here are their coordinates at start");
        //Debug.Log("[0] is " + plotMarkers[0].transform.position.x + "," + plotMarkers[0].transform.position.z);
        //Debug.Log("[1] is " + plotMarkers[1].transform.position.x + "," + plotMarkers[1].transform.position.z);
        //Debug.Log("[2] is " + plotMarkers[2].transform.position.x + "," + plotMarkers[2].transform.position.z);
        //Debug.Log("[3] is " + plotMarkers[3].transform.position.x + "," + plotMarkers[3].transform.position.z);
        


        // Sort by z
        for (int i = 0; i < plotMarkers.Length; ++i)
        {
            for (int j = 0; j < plotMarkers.Length - 1; ++j)
            {
                WorldBlockPlotBlock tempMarker = null;

                if (Mathf.RoundToInt(plotMarkers[j].transform.position.z) > Mathf.RoundToInt(plotMarkers[j + 1].transform.position.z))
                {
                    tempMarker = plotMarkers[j + 1];
                    plotMarkers[j + 1] = plotMarkers[j];
                    plotMarkers[j] = tempMarker;
                }

            }
        }

        // Sort by x
        for (int i = 0; i < plotMarkers.Length; ++i)
        {
            for (int j = 0; j < plotMarkers.Length - 1; ++j)
            {
                WorldBlockPlotBlock tempMarker = null;

                if (Mathf.RoundToInt(plotMarkers[j].transform.position.x) > Mathf.RoundToInt(plotMarkers[j + 1].transform.position.x))
                {
                    tempMarker = plotMarkers[j + 1];
                    plotMarkers[j + 1] = plotMarkers[j];
                    plotMarkers[j] = tempMarker;
                }

            }
        }

        /*
        Debug.Log("Here are markers after sorting");
        Debug.Log("[0] is " + plotMarkers[0].transform.position.x + "," + plotMarkers[0].transform.position.z);
        Debug.Log("[1] is " + plotMarkers[1].transform.position.x + "," + plotMarkers[1].transform.position.z);
        Debug.Log("[2] is " + plotMarkers[2].transform.position.x + "," + plotMarkers[2].transform.position.z);
        Debug.Log("[3] is " + plotMarkers[3].transform.position.x + "," + plotMarkers[3].transform.position.z);
        */
    }

    // Use sorted plotMarkers array to generate a list of the units the plot will manage
    // This assumes sort was successful and will likely do dumb shit otherwise
    private void GetInteriorUnits()
    {
        int xSize = Mathf.RoundToInt(plotMarkers[3].transform.position.x) - Mathf.RoundToInt(plotMarkers[1].transform.position.x) - 1;
        int zSize = Mathf.RoundToInt(plotMarkers[3].transform.position.z) - Mathf.RoundToInt(plotMarkers[2].transform.position.z) - 1;

        int xStart = Mathf.RoundToInt(plotMarkers[1].transform.position.x) + 1;
        int zStart = Mathf.RoundToInt(plotMarkers[2].transform.position.z) + 1;

        //Debug.Log("X coords: " + Mathf.RoundToInt(plotMarkers[1].transform.position.x) + " to " + Mathf.RoundToInt(plotMarkers[3].transform.position.x));

        plotUnits = new PlotUnit[xSize, zSize];

        for (int i = 0; i < plotUnits.GetLength(0); ++i)
        {
            for (int j = 0; j < plotUnits.GetLength(1); ++j)
            {
                Vector3 tempVector = new Vector3(xStart + i, 0, zStart + j);
                plotUnits[i, j] = new PlotUnit(tempVector, null);
                //Debug.Log("Added unit with these coords: " + tempVector);
            }
        }

    }

    // Till the units in plotUnits using the given delay between tillings
    // Function is a coroutine that has a delay of tillingDelay seconds
    private IEnumerator TillUnits()
    {
        // Mattock should be set to index 0 in inventory, which is hacky and bad
        inv.ChangeSelectedToIndex(0);


        for (int i = 0; i < plotUnits.GetLength(0); ++i)
        {
            for (int j = 0; j < plotUnits.GetLength(1); ++j)
            {
                //Debug.Log("Plot is using mattock for i & j: " + i + ", " + j);
                inv.UseSelected(plotUnits[i, j].dirtBlock, plotUnits[i, j].coords);

                // If new dirtBlock was created, add it to the plotUnit
                if (plotUnits[i, j].dirtBlock == null)
                {
                    // Check from center of the dirt block it should find
                    Vector3 checkVector = new Vector3(plotUnits[i, j].coords.x + 0.5f, 0, plotUnits[i, j].coords.z + 0.5f);
                    Collider[] hitColliders = Physics.OverlapBox(checkVector, new Vector3(0.25f, 1f, 0.25f));
                    if (hitColliders.Length > 0)
                    {
                        // Ignore anything hit that's not a dirt block
                        for (int k = 0; k < hitColliders.Length; ++k)
                        {
                            WorldBlockDirt foundBlock = hitColliders[k].gameObject.GetComponent<WorldBlockDirt>();
                            if (foundBlock != null)
                            {
                                plotUnits[i, j].dirtBlock = foundBlock;
                                //Debug.Log("Found the dirt block!  Yay!");
                            }
                            else
                            {
                                Debug.Log("A dirt block should have been found here");
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("Something's gone terribly wrong in the tilling code");
                    }
                }
                yield return new WaitForSeconds(tillingDelay);
            }
        } 

        if (didTillGround && !didTillDirt)
        {
            Debug.Log("Setting didTillDirt to true");
            didTillDirt = true;
        }

        if (!didTillGround)
        {
            Debug.Log("Setting didTillGround to true");
            didTillGround = true;
        }

    }
}

internal class PlotUnit
{
    internal Vector3 coords;
    internal WorldBlockDirt dirtBlock;

    internal PlotUnit(Vector3 inCoords, WorldBlockDirt inDirt)
    {
        coords = inCoords;
        dirtBlock = inDirt;
    }
}