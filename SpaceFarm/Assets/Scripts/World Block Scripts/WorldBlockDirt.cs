﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBlockDirt : WorldBlock
{
    public Material dirtBaseMat;
    public Material dirtTilledMat;

    // todo: set back to private after testing
    bool isTilled = false;
    public bool plantOnStart = false;
    bool isWatered = false;
    bool isFert = false;
    bool isPlant = false;

    //todo: remove after testing
    public Plant plantPrefab;

    private Plant childPlant;

    MeshRenderer meshRend;

    // Use this for initialization
    void Start ()
    {
        blockName = "dirt";
        meshRend = GetComponent<MeshRenderer>();
        meshRend.material = dirtBaseMat;

        if (plantOnStart)
        {
            UseMattock();
            string filePath = "plants/" + plantPrefab.GetComponent<Plant>().plantID;
            //childPlant = Instantiate(plantPrefab);
            childPlant = Instantiate(Resources.Load(filePath) as GameObject, transform).GetComponent<Plant>();
            childPlant.transform.position = transform.position;
            isPlant = true;
            UseWatering();
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    
    public override void UseMattock()
    {
        if (isTilled)
        {
            //Debug.Log("Already tilled");
            if (childPlant != null)
            {
                Debug.Log("Plant is present so attempting harvest");
                childPlant.HarvestPlant(); 
            }
        }
        else
        {
            isTilled = true;
            meshRend.material = dirtTilledMat;
        }
    }

    public override void UseWatering()
    {
        isWatered = true;
        if (childPlant != null)
        {
            childPlant.WaterPlant();
        }
        
    }

    public override void UseSeedbag(GameObject plantObj)
    {
        if (isTilled && !isPlant)
        {
            Debug.Log("Plant the seed");
            isPlant = true;
            string filePath = "plants/" + plantObj.GetComponent<Plant>().plantID;
            //childPlant = Instantiate(plantPrefab);
            Debug.Log("Loading from filepath: " + filePath);
            childPlant = Instantiate(Resources.Load(filePath) as GameObject, transform).GetComponent<Plant>();
            childPlant.transform.position = transform.position;

        if (childPlant == null)
            {
                Debug.Log("Child plant didn't instantiate");
            }

        }
        else
        {
            if (!isTilled)
            {
                Debug.Log("Dirt isn't tilled");
            }
            else
            {
                if (isPlant)
                {
                    Debug.Log("Already planted");
                }
            }

        }
    }

    public void DestroyPlant()
    {
        Destroy(transform.GetChild(0).gameObject);
        childPlant = null;
        isPlant = false;
    }

    public override void DebugDump()
    {
        base.DebugDump();
        Debug.Log("Plant growing: " + childPlant.name);
        Debug.Log("Current growth state: " + childPlant.GetGrowState());
    }

}
