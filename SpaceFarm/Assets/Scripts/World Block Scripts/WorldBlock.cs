﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBlock : MonoBehaviour
{
    public string blockName = "defaultBlockName";
    public string blockDescription = "Default block description";

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public virtual void UseMattock()
    {
        Debug.Log("Performed UseMattock on object: " + blockName);
    }

    public virtual void UseWatering()
    {
        Debug.Log("Performed UseWatering on object: " + blockName);
    }

    public virtual void UseSeedbag(GameObject plantObj)
    {
        Debug.Log("Performed UseSeedbag on object: " + blockName);
    }

    public virtual void ActivateFunction()
    {
        Debug.Log("Performed Activate on object: " + blockName);
    }

    public virtual bool TakeItem(InventoryItem inItem)
    {
        return false;
    }

    public virtual InventoryItem GiveItem()
    {
        return null;
    }

    public virtual void DebugDump()
    {
        Debug.Log("DEBUG DUMP for " + blockName);
        Debug.Log("Instance ID: " + GetInstanceID());
    }
}
