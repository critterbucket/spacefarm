﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBlockBuilding : WorldBlock
{
    public bool isContainer = false;
    public bool isAutoSort = false;
    public bool isNetworkComponent = false;
    public InventoryManager inventoryPrefab;
    public int maxSlots = 0;

    public Mesh worldMesh;

    private InventoryManager inv;

    private CharController playerRef;
    private InventoryManager playerInv;

    bool foundSomething = false;

    private RecipeLoader globalRecipeLoader;
    public bool canProcessItems = false;
    public bool canAutoProcessItems = false;
    //TODO: make this an array of recipes that can be accepted and loaded
    public string recipeName = "resourceWoodPlank0";
    private RecipeData recipeData;

    //private GameObject recipeOutput;

    //Variables for breaking block for player pickup
    public int requiredBreakHits = 3;
    private int breakHitsCounter = 0;
    public float breakHitsDecayRate = 10f;
    private float breakHitsDecayTimer;


	// Use this for initialization
	void Start ()
    {
        playerRef = GameObject.FindGameObjectWithTag("Player").GetComponent<CharController>();
        playerInv = playerRef.PassInventory();

        if (playerInv == null)
        {
            Debug.Log("Player inv not found!");
        }

        if (isContainer)
        {
            if (inventoryPrefab != null)
            {
                inv = Instantiate(inventoryPrefab, transform);
                inv.Initialize(maxSlots, isAutoSort);
            }
            else
            {
                Debug.Log("Object " + name + " tried to instantiate inventory but couldn't find prefab!");
            }
            
        }

        if (isNetworkComponent)
        {
            CheckForNetwork();
        }

        if (canProcessItems && !recipeName.Equals(""))
        {
            globalRecipeLoader = GameObject.FindWithTag("DataControl").GetComponent<RecipeLoader>();
            recipeData = globalRecipeLoader.FetchRecipe(recipeName);

            if (recipeData != null)
            {
                Debug.Log("Building " + blockName + " should now have recipe" + recipeData.recipeName + " loaded"); 
            }
            else
            {
                Debug.Log("Recipe failed to load for " + blockName);
                canAutoProcessItems = false;
            }
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Reset break hits counter after decay period is met
        if (breakHitsCounter > 0 && Time.time > breakHitsDecayTimer)
        {
            breakHitsCounter = 0;
            //Debug.Log("Resetting breakHitsCounter");
        }
		
	}

    public override void ActivateFunction()
    {
        if (isContainer)
        {
            Debug.Log("You've opened a container!  Congrats!");
            inv.OpenGUI();
        }
        else
        {
            Debug.Log("Activated WorldBlock that was not a container :(");
        }
    }

    // Command coming from ItemBuilding UseItem function.  Allows for function calls needed upon placement instead of on start
    public virtual void GetPlaced()
    {
        Debug.Log("World block placed");
    }

    // Try to add item to inventory; return false if action failed
    public override bool TakeItem(InventoryItem inItem)
    {
        bool canAdd = false;

        if (isContainer)
        {
            //Attempt to add item to inventory
            canAdd = inv.AddItem(inItem);
            if (canAdd && canAutoProcessItems)
            {
                ProcessItems();
            }
            return canAdd;
        }
        else
        {
            Debug.Log("Block isn't container and can't take items");
        }

        return base.TakeItem(inItem);
    }

    // Try to take item from inventory; return null if nothing found
    public override InventoryItem GiveItem()
    {
        if (isContainer)
        {
            // Get any items
            // Remove them and pass them back to player
            if (!inv.IsEmpty())
            {
                return inv.PopItem(false);
            }
        }
        else
        {
            Debug.Log("Block isn't container and can't give items");
        }

        return base.GiveItem();
    }

    public override void DebugDump()
    {
        base.DebugDump();
        if (inv != null)
        {
            if (!inv.IsEmpty())
            {
                inv.DebugListItems();
            }
            else
            {
                Debug.Log("Inventory is empty");
            }
        }
        
    }

    private void CheckForNetwork()
    {
        foundSomething = false;

        Debug.Log("Checking for network connections...");

        //+x direction
        CheckNetworkAlongAxis(new Vector3(1, 0, 0));

        //-x direction
        CheckNetworkAlongAxis(new Vector3(-1, 0, 0));

        //+z direction
        CheckNetworkAlongAxis(new Vector3(0, 0, 1));

        //+z direction
        CheckNetworkAlongAxis(new Vector3(0, 0, -1));

        if (foundSomething)
        {
            Debug.Log("You connected to something, dumbass");
        }

    }

    private bool CheckNetworkAlongAxis(Vector3 inAxis)
    {
        RaycastHit hitObj;
        WorldBlock hitBlock;

        float verticalOffset = 0.5f;

        Vector3 startPos = new Vector3(transform.position.x, verticalOffset, transform.position.z);

        if (Physics.Raycast(startPos, inAxis, out hitObj, 1f ))
        {
            if (hitObj.collider.gameObject.GetComponent<WorldBlockBuilding>())
            {
                hitBlock = hitObj.collider.gameObject.GetComponent<WorldBlockBuilding>();
                Debug.Log("Instance ID hit: " + hitBlock.GetInstanceID() + hitBlock.blockName);

                //todo: should only care if it's a network component
                foundSomething = true;
                return true;
            }
            else
            {
                //Debug.Log("Managed to hit something else?");
            }
        }
        else
        {
            //Debug.Log("Raycast didn't hit anything");
        }

        return false;
    }

    //Check if inventory has an item that can be processed using building's allowed recipes
    private void ProcessItems()
    {
        List<InventoryItem> heldItemIDs = inv.FetchItemList();
        Debug.Log("Attempting to process items");

        //For each element in input array, check if found in inventory
        bool hasInputItems = true;
        for (int i = 0; i < recipeData.inputList.Count; ++i)
        {
            bool foundInputItem = false;

            for (int j = 0; j < heldItemIDs.Count; j++)
            {
                Debug.Log("Checking for presence of " + recipeData.inputList[i].itemID + " against " + heldItemIDs[j].itemID);
                if (recipeData.inputList[i].itemID.Equals(heldItemIDs[j].itemID)
                    && recipeData.inputList[i].itemCountMin <= heldItemIDs[j].itemCount)
                {
                    foundInputItem = true;
                }
            }

            if (!foundInputItem)
            {
                hasInputItems = false;
                break;
            }
        }

        if (hasInputItems)
        {
            Debug.Log("You have enough things to craft that item!  Woo!");

            for (int i = 0; i < recipeData.outputList.Count; i++)
            {
                string newItemID = recipeData.outputList[i].itemID;
                Debug.Log("fetching itemID from recipe output list: " + newItemID);

                InventoryItem tempItem = Instantiate(Resources.Load(newItemID) as InventoryItem);
                if (tempItem != null)
                {
                    tempItem.itemCount = recipeData.outputList[i].itemCountMax;

                    inv.SubtractItemByCount(i, recipeData.inputList[i].itemCountMax);
                    TakeItem(tempItem);
                }
                else
                {
                    Debug.Log("Recipe output item " + newItemID + " failed to load for " + blockName);
                } 
            }

            
        }
        else
        {
            Debug.Log("You don't have required components to craft this thing");
        }
    }

    //World blocks hit with mattock will be "broken" and dropped in world for player to pick up
    public override void UseMattock()
    {
        base.UseMattock();

        ++breakHitsCounter;
        breakHitsDecayTimer = Time.time + breakHitsDecayRate;
        Debug.Log("Setting decay timer to: " + breakHitsDecayTimer);

        if (breakHitsCounter >= requiredBreakHits)
        {
            InventoryItem tempInvItem = gameObject.GetComponent<InventoryItem>();
            //TODO: loadPath may need to update if you reorg resources folder!
            string loadPath = tempInvItem.itemID;
            //InventoryItem newInvObject = Instantiate((Resources.Load(loadPath) as GameObject).GetComponent<InventoryItem>(), transform);
            //newInvObject.itemCount = tempInvItem.itemCount;
            //newInvObject.transform.parent = null;

            //playerInv.AddItem(newInvObject);
            playerInv.InstantiateAndAddItem(Resources.Load(loadPath) as GameObject, tempInvItem.itemCount);

            Debug.Log("About to destroy block: " + gameObject.name);
            Destroy(gameObject); 
        }

    }


}
