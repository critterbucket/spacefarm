﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBuilding : InventoryItem
{
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public override bool UseItem(WorldBlock hitObj, Vector3 hitPosition, InventoryItem parentItem)
    {
        if (hitObj == null)
        {
            string filePath = parentItem.itemID;
            GameObject temp = Instantiate(Resources.Load(filePath) as GameObject, transform);
            temp.transform.position = hitPosition;
            //temp.transform.parent = null;
            temp.transform.SetParent(null);
            temp.GetComponent<WorldBlockBuilding>().GetPlaced();
            return true;
        }

        return false;
        
    }

}
