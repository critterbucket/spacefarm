﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  Base class for items that can be placed in the player's inventory.  Extend it as needed
*/

public class InventoryItem : MonoBehaviour
{
    [Header("Settings visible to player")]
    public string itemName = "debug";
    public string itemDescription = "no description";

    [Header("Internal variables")]
    public int stackSize = 16;
    public string itemID = "debug";

    public int itemCount = 1;
    public bool isSingleUse = false;

    public Sprite invIcon;


	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Stand-in function for Fire1/ Use button
    public virtual bool UseItem(WorldBlock hitObj, Vector3 hitPosition, InventoryItem parentItem)
    {
        Debug.Log("Performed UseItem on object: " + itemName + ":" + GetInstanceID());
        return false;
    }

    // Standard function for Fire2/ Activate button when a WorldBlock was detected
    // Do I need this one?
    public void ActivateItem(WorldBlock hitObj)
    {
        hitObj.ActivateFunction();
    }

    // Stand-in function for Fire2/ Activate button when no WorldBlock detected
    public virtual void ActivateItem()
    {
        Debug.Log("Performed ActivateItem on object: " + itemName + ":" + GetInstanceID());
    }

}
