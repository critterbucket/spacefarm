﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemToolMattock : InventoryItem
{
    //public DirtBlockSettings dirtPrefab;
    public WorldBlockDirt dirtPrefab;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Pass on the useage information to the WorldItem that was hit, if any
    public override bool UseItem(WorldBlock hitObj, Vector3 hitPosition, InventoryItem parentItem)
    {
        if (hitObj == null)
        {
            WorldBlockDirt temp = Instantiate(dirtPrefab);
            temp.transform.position = hitPosition;
            Debug.Log("Used mattock");
        }
        else
        {
            hitObj.UseMattock();
        }

        return true;
        
    }

}
