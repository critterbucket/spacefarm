﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemToolWatering : InventoryItem
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Pass on the useage information to the WorldItem that was hit
    public override bool UseItem(WorldBlock hitObj, Vector3 hitPosition, InventoryItem parentItem)
    {
        // TODO: revise this after testing
        if (hitObj == null)
        {
            Debug.Log("Watering can hit nothing");
        }
        else
        {
            hitObj.UseWatering();
            return true;
        }

        return false;
    }

}
