﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Item Slot Panel is a panel that holds "item slot" GUI components and is controlled by an
 * InventoryManager instance
 */

public class GUIItemSlotPanel : MonoBehaviour
{
    public GameObject slot;

    public int maxSlots = 16;

    private GameObject parentPanel;
    public bool parentIsVisible;
    private GameObject[] slotList;

	// Use this for initialization
	public void Start ()
    {
        slotList = new GameObject[maxSlots];
        parentPanel = transform.parent.gameObject;

        Debug.Log("Parent: " + parentPanel);


        parentIsVisible = parentPanel.GetComponent<Canvas>().enabled;

        if (parentPanel == null)
        {
            Debug.Log("Item slot panel didn't find its parent");
        }

        for (int i = 0; i < slotList.Length; i++)
        {
            GameObject tempSlot = Instantiate(slot, transform);
            slotList[i] = tempSlot;
        }

        SetVisible(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void ActivatePanel(InventoryItem[] itemList)
    {
        bool isVisibleNow = ToggleVisible();

        if (isVisibleNow)
        {
            LoadItems(itemList);
        }

    }

    public void LoadItems(InventoryItem[] itemList)
    {
        //Debug.Log("Loading item array with length " + itemList.Length);

        int lengthMismatch = 0;

        if (slotList.Length > itemList.Length)
        {
            //TODO: should grey out excess slots
            //lengthMismatch = itemList.Length;
            lengthMismatch = slotList.Length;
            Debug.Log("Should have empty/unusable slots");
        }
        else
        {
            //TODO: should cause error
            lengthMismatch = slotList.Length;
        }

        for (int i = 0; i < lengthMismatch; ++i)
        {
            InventoryItem tempItem = null;

            //Try to fetch from the item list.  If there's something at that index, use its info to
            //update the item slot; if null, change to empty slot graphic
            try
            {
                tempItem = itemList[i];
            }
            catch (System.Exception)
            {

                tempItem = null;
            }

            InventoryItem invItem = null;

            if (tempItem != null)
            {
                invItem = itemList[i];
            }

            if (invItem == null)
            {
                UpdateSlot("0", Resources.Load<Sprite>("GUI/InventorySprites/blue_boxCross"), i);
            }
            else
            {
                UpdateSlot(invItem.itemCount.ToString(), invItem.invIcon, i);
            }

        }
    }

    //Update the slot's item count and sprite at given slotList index
    private void UpdateSlot(string newSlotCount, Sprite newSprite, int index)
    {
        // Update count
        Text slotCount = slotList[index].GetComponentInChildren<Text>();
        slotCount.text = newSlotCount;
        slotCount.transform.SetAsLastSibling();

        //Update image
        GameObject imageChild = slotList[index].transform.Find("InventorySlotIcon").gameObject;
        Image slotImage = imageChild.GetComponent<Image>();
        slotImage.overrideSprite = newSprite;
    }

    //Activates the highlight on given index and deactivates it on two nearest indices
    public void HighlightSlot(int index, int indexMax)
    {
        slotList[index].transform.Find("InventorySlotMarker").gameObject.SetActive(true);

        int indexUp, indexDown;

        if (index == indexMax)
        {
            indexUp = 0;
        }
        else
        {
            indexUp = index + 1;
        }

        if (index == 0)
        {
            indexDown = indexMax;
        }
        else
        {
            indexDown = index - 1;
        }

        slotList[indexUp].transform.Find("InventorySlotMarker").gameObject.SetActive(false);
        slotList[indexDown].transform.Find("InventorySlotMarker").gameObject.SetActive(false);
    }

    //Used to manually set parent panel's visibility
    public void SetVisible(bool shouldBeVisible)
    {
        parentIsVisible = shouldBeVisible;
        parentPanel.GetComponent<Canvas>().enabled = parentIsVisible;
    }

    //Used to toggle parent panel's visibility.  Returns updated state of parentIsVisible
    public bool ToggleVisible()
    {
        if (parentIsVisible)
        {
            parentIsVisible = false;
        }
        else
        {
            parentIsVisible = true;
        }

        parentPanel.GetComponent<Canvas>().enabled = parentIsVisible;
        return parentIsVisible;
    }
}
