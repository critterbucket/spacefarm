﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/*
 * Object definition to use for loading and editing recipes
 */

[System.Serializable]
public class RecipeDataList
{
    public List<RecipeData> recipeData;
    //public string recipeName;
   // public List<RecipeIO> inputList;
   // public List<RecipeIO> outputList;
   // public float processTime;
}

[System.Serializable]
public class RecipeData
{
    public string recipeName;
    public List<RecipeIO> inputList;
    public List<RecipeIO> outputList;
    public float processTime;
}

[System.Serializable]
public class RecipeIO
{
    public string itemID;
    public int itemCountMin;
    public int itemCountMax;
}
