﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RecipeLoader : MonoBehaviour
{
    public RecipeDataList recipeDataList;

    private string gameDataProjectFilePath = "/JSONdata/recipeData.json";

    // Use this for initialization
    void Start ()
    {
        string filePath = Application.dataPath + gameDataProjectFilePath;

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            recipeDataList = JsonUtility.FromJson<RecipeDataList>(dataAsJson);
            Debug.Log("Loaded recipe JSON file");
        }
        else
        {
            Debug.Log("Failed to find recipe JSON file!");
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Allow world blocks to pull recipe data 
    public RecipeData FetchRecipe(string inName)
    {
        Debug.Log("RecipeLoader received fetch request for: " + inName);
        RecipeData foundData = null;

        foreach (RecipeData item in recipeDataList.recipeData)
        {
            if (item.recipeName.Equals(inName))
            {
                foundData = item;
            }
        }

        return foundData;
    }
}
