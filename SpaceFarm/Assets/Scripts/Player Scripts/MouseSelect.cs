﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSelect : MonoBehaviour
{
    public int gridSize = 2;

    int selectorHeight = 0; // Y value on map
    bool isActive = true;

    GameObject collidingWith = null;

    // Use this for initialization
    void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {
        // I don't know how tf this works but it seems to be great?
        // Move selector to mouse position and lock to grid
        if (isActive)
        {

            Vector3 mouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
            Vector3 screenPoint = Camera.main.ScreenToWorldPoint(mouse);
            Vector3 cameraForward = Camera.main.transform.forward;
            float k = (selectorHeight - screenPoint.y) / cameraForward.y;
            screenPoint += cameraForward * k;
            //transform.position = screenPoint;
            transform.position = new Vector3(Mathf.Round(screenPoint.x / gridSize) * gridSize,
                screenPoint.y,
                Mathf.Round(screenPoint.z / gridSize) * gridSize);

        }
    }

    public GameObject CheckCollision()
    {
        return collidingWith;
    }

    void OnTriggerEnter(Collider col)
    {
        collidingWith = col.gameObject;
    }

    void OnTriggerExit(Collider col)
    {
        collidingWith = null;
    }
}
