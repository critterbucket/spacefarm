﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour
{
    [SerializeField] float baseMoveSpeed = 4f;
    public MouseSelect gridMarker;
    float clickDelay = 0.3f;
    float buttonDelay = 0.3f;

    // Inventory manager
    InventoryManager inv;

    // Hotbar
    //GUIItemHotbarPanel hotbar;

    Vector3 forward, right;
    Vector3 rightMovement, upMovement;
    float moveSpeed;

    float actionButtonDelayTracker;
    float toolButtonDelayTracker;
    float interactButtonDelayTracker;

    // Use this for initialization
    void Start ()
    {
        inv = GameObject.FindWithTag("InventoryManager").GetComponent<InventoryManager>();
        if (inv == null)
        {
            Debug.Log("Failed to find inventory manager");
        }
        else
        {
            inv.InitializeForPlayer();
        }

        //todo: add try/catch for public vars (whu?)
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);  //what?  Something about motion?
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;  //gets an angle 90deg from camera forward
        if(gridMarker == null)
        {
            Debug.Log("OOPS-- Missing GridMarker!");
        }

        //TODO: remove
        //hotbar = GameObject.FindWithTag("HotbarPanel").GetComponent<GUIItemHotbarPanel>();
        //hotbar.LoadHotbar(inv.FetchGameObjectList());

	}
	
	// Update is called once per frame
	void Update ()
    {
		// WASD and joystick buttons
        if(Input.GetAxis("HorizontalKey") != 0 || Input.GetAxis("VerticalKey") != 0)
        {
            Move();
        }

        // Switch tool button
        if(Input.GetAxis("DPadHorizontal") != 0 && Time.time >= toolButtonDelayTracker)
        {
            inv.ChangeSelected(Input.GetAxis("DPadHorizontal"));
            toolButtonDelayTracker = Time.time + buttonDelay;
        }

        // Click/ action button
        if (Input.GetAxis("Fire1") != 0 && Time.time >= actionButtonDelayTracker)
        {
            GameObject hitObject = gridMarker.CheckCollision();
            /*
            if (hitObject != null && hitObject.gameObject.GetComponent<WorldBlock>())
            {
                inv.UseSelected(hitObject.GetComponent<WorldBlock>(), gridMarker.transform.position);
            }
            else
            {
                if (hitObject == null)
                {
                    inv.UseSelected(null, gridMarker.transform.position);
                }
                else
                {
                    Debug.Log("Game hit something that wasn't a world block: " + hitObject.name);
                }
               
            }
            */

            if (hitObject != null)
            {
                if (hitObject.gameObject.GetComponent<WorldBlock>())
                {
                    Debug.Log("Hit a world block");
                    inv.UseSelected(hitObject.GetComponent<WorldBlock>(), gridMarker.transform.position);
                }
                else
                {
                    if (hitObject.gameObject.GetComponent<Plant>())
                    {
                        Debug.Log("Hit a plant, fetching parent block");
                        WorldBlock tempBlock = hitObject.transform.parent.GetComponent<WorldBlock>();
                        inv.UseSelected(tempBlock, gridMarker.transform.position);
                    }
                }
            }
            else
            {
                inv.UseSelected(null, gridMarker.transform.position);

            }

            actionButtonDelayTracker = Time.time + clickDelay;

        }

        // Interact button
        if (Input.GetAxis("Fire2") != 0
            && Time.time >= interactButtonDelayTracker
            && Input.GetAxis("Shift") == 0)
        {
            GameObject hitObject = gridMarker.CheckCollision();
            if (hitObject != null && hitObject.gameObject.GetComponent<WorldBlock>())
            { 
                inv.ActivateSelected(hitObject.GetComponent<WorldBlock>());
            }
            else
            {
                if (hitObject == null)
                {
                    inv.ActivateSelected(null);
                }
                else
                {
                    Debug.Log("Game hit something that wasn't a world block");
                }

            }

            interactButtonDelayTracker = Time.time + clickDelay;
        }

        // Shift-interact button
        if (Input.GetAxis("Fire2") != 0
            && Time.time >= interactButtonDelayTracker
            && Input.GetAxis("Shift") != 0)
        {
            GameObject hitObject = gridMarker.CheckCollision();
            if (hitObject != null && hitObject.gameObject.GetComponent<WorldBlock>())
            {
                inv.QuickActivate(hitObject.GetComponent<WorldBlock>());
            }

            interactButtonDelayTracker = Time.time + clickDelay;

        }

        // DEBUG: Q key to give item info dump in log
        //TODO: prob want to change this when inventory panel is working
        if (Input.GetButton("Debug")
            && Time.time >= interactButtonDelayTracker)
        {
            Debug.Log("Hit debug button");
            GameObject hitObject = gridMarker.CheckCollision();
            //if (hitObject != null && hitObject.gameObject.GetComponent<WorldBlock>())
            //{
                //inv.DebugDump(hitObject.GetComponent<WorldBlock>());

                inv.OpenGUI();
            //}

            interactButtonDelayTracker = Time.time + buttonDelay;
        }


    }

    // Using rotation of X and Z axes calculated in Start, calculate movement for both axes and normalize
    // to create one Vector3 to use for movement and rotation
    void Move()
    {
        // If both inputs active, reduce baseMoveSpeed to compensate for diagonal
        if(Input.GetAxis("HorizontalKey") != 0 && Input.GetAxis("VerticalKey") != 0)
        {
            moveSpeed = baseMoveSpeed * 0.71f;
        }
        else
        {
            moveSpeed = baseMoveSpeed;
        }

        rightMovement = right * moveSpeed * Time.deltaTime * Input.GetAxis("HorizontalKey");
        upMovement = forward * moveSpeed * Time.deltaTime * Input.GetAxis("VerticalKey");

        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);

        transform.forward = heading; //for rotation
        transform.position += rightMovement; //for location
        transform.position += upMovement; // for location

        Camera.main.transform.position += rightMovement;
        Camera.main.transform.position += upMovement;
    }

    public InventoryManager PassInventory()
    {
        return inv;
    }

}
