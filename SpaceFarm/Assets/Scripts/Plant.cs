﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{
    public string plantName = "debugPlant";
    public string plantDesc = "debugPlant description";
    public string plantID;

    [Header("Type and Amount of Drop")]
    public GameObject dropTypeObj;
    private InventoryItem dropType;
    public int dropCount = 1;

    //public int growTime = 5;
    float growStartTime = 0;
    private float growthTimeElapsed = 0;  //always start at zero to correctly offset elapsed growth vs growth start time
    [Header("Manual Placement")]
    public bool canGrow = false;
    public bool isWatered = false;

    [Header("Growth Time Per Stage")]
    public int seedlingGrowTime = 0;
    public int sproutGrowTime = 0;
    public int growthGrowTime = 0;
    public int matureGrowTime = 0;

    [Header("Growth Stage Meshes")]
    public Mesh seedlingMesh;
    public Mesh sproutMesh;
    public Mesh growthMesh;
    public Mesh matureMesh;
    public Mesh bearingMesh;

    private CharController playerRef;
    private InventoryManager playerInv;

    private MeshFilter meshFilterRef;

    public enum GrowStateEnum
    {
        seedling,
        sprout,
        growth,
        mature,
        bearing
    }
    private GrowStateEnum plantState;

    [Header("Regrowth Variables")]
    public bool canRegrow = true;
    public GrowStateEnum stateAfterHarvest;

    // Use this for initialization
    void Start ()
    {
        //canGrow = false;
        playerRef = GameObject.FindGameObjectWithTag("Player").GetComponent<CharController>();
        playerInv = playerRef.PassInventory();

        meshFilterRef = GetComponent<MeshFilter>();
        if (seedlingMesh != null)
        {
            meshFilterRef.mesh = seedlingMesh;
        }
        else
        {
            Debug.Log("Plant missing seedling mesh!  Failed to load mesh on start");
        }

        dropType = dropTypeObj.GetComponent<InventoryItem>();
        if (dropType == null)
        {
            Debug.Log("Plant didn't load drop type");
        }

    }

    /* Use this to instantiate any variables that need to be set immediately upon instantiate, otherwise they
     * might get overwritten later
    */
    private void Awake()
    {
        canGrow = false;
    }

    //Update is called once per frame
    //Whether or not the plant moves on to the next stage is based on Time.time, time the plant started growing, and total elapsed time since
    // growth started
    void Update ()
    {
        if (isWatered && canGrow)
        {
            switch (plantState)
            {
                case GrowStateEnum.seedling:
                    {
                        if (Time.time >= growStartTime + seedlingGrowTime)
                        {
                            plantState = GrowStateEnum.sprout;
                            //Debug.Log("Plant " + plantName + " grew to " + plantState + " at " + Time.time + " and should have" +
                            //    " waited " + seedlingGrowTime + " seconds");
                            growthTimeElapsed += seedlingGrowTime;

                            if (sproutMesh != null)
                            {
                                meshFilterRef.mesh = sproutMesh;
                            }
                        }
                        break;
                    }
                case GrowStateEnum.sprout:
                    {
                        if (Time.time >= growStartTime + sproutGrowTime + growthTimeElapsed)
                        {
                            plantState = GrowStateEnum.growth;
                            //Debug.Log("Plant " + plantName + " grew to " + plantState + " at " + Time.time + " and should have" +
                            //    " waited " + sproutGrowTime + " seconds");
                            growthTimeElapsed += sproutGrowTime;

                            if (growthMesh != null)
                            {
                                meshFilterRef.mesh = growthMesh;
                            }
                        }
                        break;
                    }
                case GrowStateEnum.growth:
                    {
                        if (Time.time >= growStartTime + growthGrowTime + growthTimeElapsed)
                        {
                            plantState = GrowStateEnum.mature;
                            //Debug.Log("Plant " + plantName + " grew to " + plantState + " at " + Time.time + " and should have" +
                            //    " waited " + growthGrowTime + " seconds");
                            growthTimeElapsed += growthGrowTime;
                            //transform.localScale += new Vector3(0, 0.5f, 0);

                            if (matureMesh != null)
                            {
                                meshFilterRef.mesh = matureMesh;
                            }
                        }
                        break;
                    }
                case GrowStateEnum.mature:
                    {
                        if (Time.time >= growStartTime + matureGrowTime + growthTimeElapsed)
                        {
                            plantState = GrowStateEnum.bearing;
                            //Debug.Log("Plant " + plantName + " grew to " + plantState + " at " + Time.time + " and should have" +
                            //    " waited " + matureGrowTime + " seconds");
                            growthTimeElapsed += matureGrowTime;

                            if (bearingMesh != null)
                            {
                                meshFilterRef.mesh = bearingMesh;
                            }
                        }
                        break;
                    }
                case GrowStateEnum.bearing:
                    {
                        Debug.Log("Plant's already grown, so stop it");
                        canGrow = false;
                        break;
                    }
                default:
                    {
                        //todo: should bearing be the default here?
                        Debug.Log("Plant " + plantName + " had invalid GrowStateEnum somehow!");
                        Debug.Log("Defaulting to bearing state...");
                        plantState = GrowStateEnum.bearing;
                        canGrow = false;
                        meshFilterRef.mesh = bearingMesh;
                        break;
                    }
            } 
        }

    }

    public void WaterPlant()
    {
        if (!isWatered)
        {
            growStartTime = Time.time;
            Debug.Log("Plant " + plantName + "started growing at " + growStartTime);
            canGrow = true;
            isWatered = true;
        }
        else
        {
            Debug.Log("Plant already watered");
        }
    }

    public void HarvestPlant()
    {
        //todo: add conditions for if plant can't regrow after harvest or if it reverts to a different state upon harvest
        if (plantState.Equals(GrowStateEnum.bearing))
        {
            Debug.Log("Harvesting plant");

            if (canRegrow)
            {
                if (stateAfterHarvest.Equals(GrowStateEnum.seedling))
                {
                    plantState = GrowStateEnum.seedling;
                    //growCurrentTime = 0;

                    if (seedlingMesh != null)
                    {
                        meshFilterRef.mesh = seedlingMesh;
                    }
                    else
                    {
                        Debug.Log("Plant missing seedling mesh!  Failed to reset mesh");
                    }
                }
                else
                {
                    plantState = stateAfterHarvest;
                    //Debug.Log("Regrowing plant from " + plantState.ToString());
                }

                if (isWatered)
                {
                    growStartTime = Time.time;
                    canGrow = true;
                    growthTimeElapsed = 0;
                    //Debug.Log("Regrowing plant from " + plantState.ToString() + " starting at " + growStartTime + " with elapsed time " + growthTimeElapsed);
                } 
            }

            string filePath = dropType.itemID;
            playerInv.InstantiateAndAddItem(Resources.Load(filePath) as GameObject, dropCount);

            if (!canRegrow)
            {
                //tell parent that you're going away
                transform.GetComponentInParent<WorldBlockDirt>().DestroyPlant();
                //Destroy(gameObject);
            }

        }
        else
        {
            Debug.Log("Plant not ready to harvest");
        }

    }

    public string GetGrowState()
    {
        return plantState.ToString();
    }

}
